# Gateway API

Gerenciador de Microsserviços que utiliza distintas formas de autenticação entre microsserviços.

## Pré-requisito
-  Python 3
-  Flask Framework
-  SQL Alchemy

## Instalar
```
pip install requirements.txt
```

## Autor

Diego Feitosa - diegusmiestro@gmail.com

## Licença
[MIT licensed](https://pt.wikipedia.org/wiki/Licen%C3%A7a_MIT)